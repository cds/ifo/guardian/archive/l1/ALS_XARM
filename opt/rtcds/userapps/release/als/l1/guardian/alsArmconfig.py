#######
#ALS ARM PARAMETERS FILE
#######
# Tested values good for HWS
#pos_yawoffset4tcs = {'ALS_XARM':0.80,'ALS_YARM':0.60}
#ang_yawoffset4tcs = {'ALS_XARM':0.50,'ALS_YARM':0.50}
#Trial values for working transistion
pos_pitoffset4tcs = {'ALS_XARM':-0.20,'ALS_YARM':-0.40}
ang_pitoffset4tcs = {'ALS_XARM':0.00,'ALS_YARM':0.00}
pos_yawoffset4tcs = {'ALS_XARM':-0.20,'ALS_YARM':0.00}
ang_yawoffset4tcs = {'ALS_XARM':-0.50,'ALS_YARM':0.60}

#pos_pitoffset4tcs = {'ALS_XARM':0.80,'ALS_YARM':0.80}
#ang_pitoffset4tcs = {'ALS_XARM':0.80,'ALS_YARM':0.80}
#pos_yawoffset4tcs = {'ALS_XARM':0.60,'ALS_YARM':0.80}
#ang_yawoffset4tcs = {'ALS_XARM':0.60,'ALS_YARM':0.80}

#Nominal values for normal operation 
#pos_yawoffset4servo = {'ALS_XARM':0.60,'ALS_YARM':-0.30} #AJM changed to match nominal IR alignment 161020
pos_pitoffset4servo = {'ALS_XARM':0.40,'ALS_YARM':0.3}		#XARM was 0.3 200707 #YARM was 0.4 20210301
ang_pitoffset4servo = {'ALS_XARM':0.00,'ALS_YARM':0.00}
pos_yawoffset4servo = {'ALS_XARM':0.40,'ALS_YARM':-0.3}	#YARM was 0.1 20230106
ang_yawoffset4servo = {'ALS_XARM':0.00,'ALS_YARM':-0.3}	#YARM was -0.1 20230106


#Servo settings
pdh_acq_gain = {'ALS_XARM':-5,'ALS_YARM':1}    #YARM decreased from 5dB to 1dB (AJM211105),  XARM increased from -9 to -5dB (AJM240403)
pdh_lock_gain = {'ALS_XARM':0,'ALS_YARM':2}	#YARM decreased from 9dB to 5dB (AJM211109), decreased again 5dB to 2dB (AJM220616). XARM increased from -4 to 0dB (AJM240403)

#Threshold for locking PDH
#transpd_threshold = {'ALS_XARM':700,'ALS_YARM':600}
tpd_threshold = {'ACQUIRE':{'ALS_XARM':700,'ALS_YARM':600},
                 'ALIGNED':{'ALS_XARM':1000,'ALS_YARM':900},
                 }
#lowered YARM thresholds from (ACQUIRE) 700 to 600 and (ALIGNED) 1000 to 900

#transpd_threshold = {'COMM':-1,'DIFF':-27} # TH 180926 changed to monitor diff

#Beat note frequency for PLL
beatnote_set = {'ALS_XARM':-39.6,'ALS_YARM':79.2}


drive_mtrx = {'P':[[-1.07,1.0],
                     [-1.0,0.78]],
                'Y':[[1.07,1.0],
                     [-1.0,-0.78]]
               }


pll_autolocker = {'ALS_XARM':1,'ALS_YARM':1}

#TMS ADS servo settings
tms_osc_number = {'ALS_XARM':9,'ALS_YARM':10}
#tms_demod_sensor = {'ALS_XARM':8,'ALS_YARM':9}
#tms_servo_row = {'ALS_XARM':11,'ALS_YARM':12}

tms_osc_freq = {'ALS_XARM':{'PIT':2.3,'YAW':3.5},
                'ALS_YARM':{'PIT':2.9,'YAW':4.1}
                }

tms_demod_phase = {'ALS_XARM':{'PIT':132,'YAW':93},
                   'ALS_YARM':{'PIT':111,'YAW':-3}
                   }

# Division by 10 because of gain of 10 in integrator in TMS bank
tms_dof_gain = {'ALS_XARM':{'PIT':1.5/10,'YAW':1.5/10},
                'ALS_YARM':{'PIT':0.8/10,'YAW':1.1/10}
               }
#tms_dof_gain = {'ALS_XARM':{'PIT':1.5/10,'YAW':1.5/10},
#                'ALS_YARM':{'PIT':0.7/10,'YAW':1.0/10}
#               }

#tms_osc_row_dict = {'ALS_XARM':,'ALS_YARM':}

### Camera servo settings
cam_dofs = {'ALS_XARM':{'ITM':11,'ETM':13},
            'ALS_YARM':{'ITM':12,'ETM':14}
            }


# Off-center values

# Updated 2024-12-09
cam_offsets = {'ALS_XARM':
                 {'ITM':{'PIT':-20.0,'YAW':6.0},
                  'ETM':{'PIT':-4.5,'YAW':1.4}},
               'ALS_YARM':
                 {'ITM':{'PIT':-15.0,'YAW':-5.0},
                  'ETM':{'PIT':1.4,'YAW':10}}
                }

'''
cam_offsets = {'ALS_XARM':
                 {'ITM':{'PIT':-20.0,'YAW':6.0},
                  'ETM':{'PIT':-4.5,'YAW':1.4}},
               'ALS_YARM':
                 {'ITM':{'PIT':-15.0,'YAW':-16.0},
                  'ETM':{'PIT':1.4,'YAW':10}}
                }


cam_offsets = {'ALS_XARM':
                 {'ITM':{'PIT':-20.0,'YAW':6.0},
                  'ETM':{'PIT':-4.5,'YAW':1.4}},
               'ALS_YARM':
                 {'ITM':{'PIT':-16.0,'YAW':-19.0},
                  'ETM':{'PIT':1.4,'YAW':10}}
                }
'''

# AJM240917 - ITM{X,Y} vals changed following camera refocusing work


'''
cam_offsets = {'ALS_XARM':
                 {'ITM':{'PIT':-15.0,'YAW':33.0},
                  'ETM':{'PIT':-4.5,'YAW':1.4}},
               'ALS_YARM':
                 {'ITM':{'PIT':-6.2,'YAW':-4.0},
                  'ETM':{'PIT':1.4,'YAW':10}}
                }
'''
# AJM24xxxx - XARM ITM YAW changed from 23 to 37

'''
cam_offsets = {'ALS_XARM':
                 {'ITM':{'PIT':-15.0,'YAW':23.0},
                  'ETM':{'PIT':-4.5,'YAW':1.4}},
               'ALS_YARM':
                 {'ITM':{'PIT':-6.2,'YAW':-4.0},
                  'ETM':{'PIT':1.4,'YAW':10}}
                }
'''
# AJM230805 - YARM ITM YAW changed from -16 to -10, then -8 (next day), then -4

'''
cam_offsets = {'ALS_XARM':
                 {'ITM':{'PIT':-15.0,'YAW':0},
                  'ETM':{'PIT':-4.5,'YAW':1.4}},
               'ALS_YARM':
                 {'ITM':{'PIT':-6.2,'YAW':2.5},
                  'ETM':{'PIT':1.4,'YAW':10.0}}
                }
'''

#ITMX:  YAW was 0, now 12
#ITMY:  PIT was -6.2, now -13;  YAW was 2.5, now -11
#ETMY:  PIT was 1.4, now 10.0;  YAW was 10.0, now -2.0 

cam_gains = {'ALS_XARM':
               {'ITM':{'PIT':300,'YAW':400},
                'ETM':{'PIT':300,'YAW':400}},
             'ALS_YARM':
               {'ITM':{'PIT':300,'YAW':400},
                'ETM':{'PIT':300,'YAW':400}},
}


sus_cals = {'ALS_XARM':{'TMS':{'P':1.0/200,'Y':1.0/200},
                        'ITM':{'P':1.8/400,'Y':1.15/400},
                        'ETM':{'P':1.6/400,'Y':1.2/400}},
            'ALS_YARM':{'TMS':{'P':1.0/200,'Y':1.0/200},
                        'ITM':{'P':1.8/400,'Y':1.3/400},
                        'ETM':{'P':1.6/400,'Y':1.2/400}}
            }


alignment_scanner = {'ALS_XARM':True,
                      'ALS_YARM':True
                     }

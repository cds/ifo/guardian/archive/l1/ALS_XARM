# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

import time, alsArmconfig
import cdsutils
from guardian import GuardState, GuardStateDecorator
from laserFreqScan import LaserFreqScan
import numpy as np

import runningAvg
from isclib.epics_average import EzAvg, ezavg
import isclib.matrices as matrix

import awg
import gpstime

# TODO: Decorator for PLL
# TODO: Decorator for Laser
# TODO: Check to see that QPDs are actually locked at the correct position

nominal = 'SHUTTERED'

##################################################

def shutter_ctrl(cmd):
    """OPEN or CLOSE green shutter"""
    channel = ':SYS-MOTION_'+ARM+'_SHUTTER_A_'+cmd
    ezca[channel] = 1

def pdh_ctrl(cmd,boost=True):
    """ON or OFF for PDH servo"""
    if cmd == 'ON':
        v = 1
    elif cmd == 'OFF':
        v = 0
    else:
        raise ValueError()
    ezca['REFL_SERVO_IN1EN'] = v
    time.sleep(0.6)
    ezca['REFL_SERVO_COMCOMP'] = v
    time.sleep(0.6)
    if boost:
        ezca['REFL_SERVO_COMBOOST'] = v

def QPDs_bright():
    sum_threshold = 100
    #chans = map(lambda x: 'ALS-'+ARM+'_QPD_{var}_SUM_OUTPUT'.format(var=x),['A','B'])
    #time.sleep(1)
    #qpd_sums = cdsutils.avg(-1,chans,stddev=False)
    #return all(i > sum_threshold for i in qpd_sums)
    qpd_sums = []
    for qpd in ['A','B']:
        qpd_sums.append(ezca['QPD_'+qpd+'_NSUM_OUTPUT'])
    return all(i > sum_threshold for i in qpd_sums)


#def QPDs_centered():
#    r_max = 0.7
#    chans = map(lambda x: 'ALS-'+ARM+'_QPD_{var}_OUTPUT'.format(var=x),['A_PIT','B_PIT','A_YAW','B_YAW'])
#    time.sleep(1)
#    qpd_vals = cdsutils.avg(-1,chans,stddev=False)
#    return all(i < r_max for i in qpd_vals)

#def TransPD_bright():
#    channel = 'ALS-C_TR'+ARM+'_A_LF_OUTPUT'
#    transpd_val = cdsutils.avg(-3,channel,stddev=False)
#    return transpd_val > alsArmconfig.transpd_threshold[SYSTEM]

def pmc_unlocked():
    pmc_trans_th = 30
    pmc_trans = ezca[':PSL-PWR_PMC_TRANS_OUTPUT']
    return pmc_trans < pmc_trans_th
    

def Beatnote_good():
    beatampl_chan = 'FIBR_A_DEMOD_RFMON'
    beatampl_val = ezca[beatampl_chan]
    #log(beatampl_val)
    return beatampl_val>-28     #dBm

def BeatnoteFreq_good():
    beatfreq_chan = 'FIBR_LOCK_BEAT_FREQUENCY'
    beatfreq_val = ezca[beatfreq_chan]
    return abs(beatfreq_val-abs(alsArmconfig.beatnote_set[SYSTEM]))<5.0

def PLL_locked():
    return (ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] and ezca['FIBR_SERVO_IN1EN'])

#def PZT_output_off():
#     for pzt in ['1','2']:
#         for dof in ['PIT','YAW']:
#             if ezca["PZT{}_{}_GAIN".format(pzt,dof)]==0:
#                 return True
#     return
    
def laser_on():
    return (ezca['LASER_GR_LF_OUTPUT']) > 100


def turn_off_qpd_servos():

    #Hold the PZT outputs
    ezca.switch('PZT1_PIT','HOLD','ON')
    ezca.switch('PZT1_YAW','HOLD','ON')
    ezca.switch('PZT2_PIT','HOLD','ON')
    ezca.switch('PZT2_YAW','HOLD','ON')
    time.sleep(0.1)

    # Turn off offsets and inputs
    for dof1 in ['POS','ANG']:
        for dof2 in ['PIT','YAW']:
            ezca['IP_{}_{}_TRAMP'.format(dof1,dof2)] = 0
            time.sleep(0.1)
            ezca.switch('IP_{}_{}'.format(dof1,dof2),'OFFSET','INPUT', 'OFF')
    time.sleep(1)

    # Clear histories
    for dof1 in ['POS','ANG']:
        for dof2 in ['PIT','YAW']:
            ezca['IP_{}_{}_RSET'.format(dof1,dof2)] = 2


def turn_off_pll():
    ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0
    ezca['FIBR_SERVO_IN1EN'] = 0

##################################################
# Decorators
##################################################
class assert_qpds_bright(GuardStateDecorator):
    def pre_exec(self):
        if not QPDs_bright():
            log('no light on QPDs')
            return 'RESET_QPD_SERVOS'

#class assert_trans_bright(GuardStateDecorator):
#    def pre_exec(self):
#        if not TransPD_bright():
#            log('Light level low on Trans PD')
#            return ''

class assert_pll_locked(GuardStateDecorator):
    def pre_exec(self):
        if not alsArmconfig.pll_autolocker[SYSTEM]:
            return
        if not (ezca['FIBR_LOCK_LOGIC_ENABLE'] == 0):
            ezca['FIBR_LOCK_LOGIC_ENABLE'] = 0
        if not Beatnote_good():
            if pmc_unlocked():
                log('PMC is unlocked')
                return 'WAIT_FOR_PMC'
            log('Fiber PLL beat too far away, beginning Coarse Scan')
            return 'COARSE_SCAN_PLL'
        elif not BeatnoteFreq_good():
            log('Fiber PLL close but still too far away, beginning Fine Scan')
            return 'FINE_SCAN_PLL'
        elif not PLL_locked():
            log('Fiber PLL servo is off, turning on')
            return 'LOCK_PLL'

class laser_checker(GuardStateDecorator):
    def pre_exec(self):

        if not laser_on():
             turn_off_qpd_servos()
             pdh_ctrl('OFF')
             turn_off_pll()
             turn_off_align_servos()
             return 'LASER_OFF'

class pmc_checker(GuardStateDecorator):
    def pre_exec(self):

        if pmc_unlocked():
            log('PMC not locked, turning off PLL')
            turn_off_pll()
            pdh_ctrl('OFF')

        if not laser_on():
            log('Laser off, turning off PLL')
            turn_off_pll()
            pdh_ctrl('OFF')

##################################################
##################################################
class INIT(GuardState):
    request = True



##################################################
class UNLOCK_PDH(GuardState):
    index = 80
    request = False

    def main(self):
        pdh_ctrl('OFF')
        #Change offsets for HWS measurement - ajm and cdb 171107
        #ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4tcs[SYSTEM]
        #ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4tcs[SYSTEM]
        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4tcs[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4tcs[SYSTEM]

        # Moved the ISC_lock guardian CDB 2017 Nov 10
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_TRAMP'] = 10
        #time.sleep(0.5)
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_OFFSET'] += 4

        return True

##################################################
class UNLOCK_PDH_GOTO(GuardState):
    index = 81
    request = False
    goto = True

    @laser_checker
    @pmc_checker
    def main(self):
        pdh_ctrl('OFF')
        #Change offsets for HWS measurement - ajm and cdb 171107
        #ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4tcs[SYSTEM]
        #ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4tcs[SYSTEM]
        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4tcs[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4tcs[SYSTEM]

        # Moved the ISC_lock guardian CDB 2017 Nov 10
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_TRAMP'] = 10
        #time.sleep(0.5)
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_OFFSET'] += 4

        return True


##################################################
class TURN_OFF_QPD_SERVOS(GuardState):
    request = False
    index = 90

    @laser_checker
    @pmc_checker
    def main(self):

        turn_off_qpd_servos()

        # Set TRAMPS
        #ezca['IP_POS_PIT_TRAMP'] = 3
        #ezca['IP_ANG_PIT_TRAMP'] = 3
        #ezca['IP_POS_YAW_TRAMP'] = 3
        #ezca['IP_ANG_YAW_TRAMP'] = 3
        #time.sleep(0.3)

        # Turn off offsets
        #ezca.switch('IP_POS_PIT', 'OFFSET', 'OFF')
        #ezca.switch('IP_ANG_PIT', 'OFFSET', 'OFF')
        #ezca.switch('IP_POS_YAW', 'OFFSET', 'OFF')
        #ezca.switch('IP_ANG_YAW', 'OFFSET', 'OFF')
        #time.sleep(3.5)

        # Turn off inputs
        #ezca.switch('IP_POS_PIT', 'INPUT', 'OFF')
        #ezca.switch('IP_ANG_PIT', 'INPUT', 'OFF')
        #ezca.switch('IP_POS_YAW', 'INPUT', 'OFF')
        #ezca.switch('IP_ANG_YAW', 'INPUT', 'OFF')

    #@laser_checker
    @pmc_checker
    def run(self):

        return True

##################################################

class UNLOCK_PLL(GuardState):
    request = False
    index = 93


    @pmc_checker
    def main(self):

        log('Turning off PLL')
        turn_off_pll()

    @pmc_checker
    def run(self):

        return True


##################################################
class SHUTTER_GREEN(GuardState):
    index = 95
    request = False

    @pmc_checker
    def main(self):
        shutter_ctrl('CLOSE')
        #test als misalignemt for HWS 
        # Turn off PDH
        pdh_ctrl('OFF')
        time.sleep(1)
        # Set ramp times
        for dof1 in ['POS', 'ANG']:
            for dof2 in ['PIT', 'YAW']:
                ezca['IP_' + dof1 + '_' + dof2 + '_TRAMP'] = 5.0

        #ezca.switch('IP_POS_YAW', 'OFFSET', 'ON')
        #ezca.switch('IP_ANG_YAW', 'OFFSET', 'ON')

        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4tcs[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4tcs[SYSTEM]

        time.sleep(5)
    	# TODO:
        # Make a new state for ETM HWS alignment 
        # Then in shutter turn off QPD servos and PDH Loop

        return True

##################################################
class SHUTTERED(GuardState):
    index = 100

    def main(self):
        #Ensure we don't have any rogue 
        awg.awg_cleanup()
        return True

    @pmc_checker
    def run(self):

        return True

##################################################
class PZT_MIRRORS_OFF(GuardState):
    index = 110
    request = True

    @pmc_checker
    def main(self):

        tramp = 10

        #Don't hold the PZT outputs, so they can be ramped down
        ezca.switch('PZT1_PIT','HOLD','OFF')
        ezca.switch('PZT1_YAW','HOLD','OFF')
        ezca.switch('PZT2_PIT','HOLD','OFF')
        ezca.switch('PZT2_YAW','HOLD','OFF')
        time.sleep(0.1)


        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_TRAMP'] = tramp
        time.sleep(0.5)

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_GAIN'] = 0

        self.timer['pztsoff'] = tramp

    @pmc_checker
    def run(self):

        if not self.timer['pztsoff']:
            return
        return True

##################################################
class PZT_MIRRORS_ON(GuardState):
    index = 120
    request = False

    @pmc_checker
    def main(self):

        tramp = 10

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_TRAMP'] = tramp
        time.sleep(0.5)

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_GAIN'] = 1

        self.timer['pztson'] = tramp

    @pmc_checker
    def run(self):

        if not self.timer['pztson']:
            return
        return True

##################################################
class LASER_OFF(GuardState):
    index = 5
    request = False

    def run(self):

        if not laser_on():
            notify('ALS laser is off')
            return

        return True

##################################################
class RELEASE_GREEN_PHOTONS(GuardState):
    index = 10
    request = False

    @laser_checker
    @pmc_checker
    def main(self):
        shutter_ctrl('OPEN')
        self.timer['photons'] = 5

    @laser_checker
    @pmc_checker
    def run(self):

        notify('PZT outputs are held, move to next state to release')

        if not self.timer['photons']:
            return

        return True


##################################################
class RESET_QPD_SERVOS(GuardState):
    index = 16
    request = False

    @laser_checker
    @pmc_checker
    def main(self):

        #TODO include checker for ALS PZT filters

        # Reset PZT offsets for als alignemnt
        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4servo[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4servo[SYSTEM]
        #ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4servo[SYSTEM]
        #ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4servo[SYSTEM]

        # Turn off pos yaw offset
        ezca['IP_POS_PIT_TRAMP'] = 0
        ezca['IP_ANG_PIT_TRAMP'] = 0
        ezca['IP_POS_YAW_TRAMP'] = 0
        ezca['IP_ANG_YAW_TRAMP'] = 0

        time.sleep(0.3)

        ezca.switch('IP_POS_PIT', 'OFFSET', 'OFF')
        ezca.switch('IP_ANG_PIT', 'OFFSET', 'OFF')
        ezca.switch('IP_POS_YAW', 'OFFSET', 'OFF')
        ezca.switch('IP_ANG_YAW', 'OFFSET', 'OFF')

        # Turn off inputs
        ezca.switch('IP_POS_PIT', 'INPUT', 'OFF')
        ezca.switch('IP_ANG_PIT', 'INPUT', 'OFF')
        ezca.switch('IP_POS_YAW', 'INPUT', 'OFF')
        ezca.switch('IP_ANG_YAW', 'INPUT', 'OFF')

        # Turn off 20dB
        ezca.switch('IP_POS_PIT', 'FM6', 'OFF')
        ezca.switch('IP_ANG_PIT', 'FM6', 'OFF')
        ezca.switch('IP_POS_YAW', 'FM6', 'OFF')
        ezca.switch('IP_ANG_YAW', 'FM6', 'OFF')

        time.sleep(0.3)

        # Clear histories
        ezca['IP_POS_PIT_RSET'] = 2
        ezca['IP_ANG_PIT_RSET'] = 2
        ezca['IP_POS_YAW_RSET'] = 2
        ezca['IP_ANG_YAW_RSET'] = 2

        # Stop holding PZT outputs
        ezca.switch('PZT1_PIT','HOLD','OFF')
        ezca.switch('PZT1_YAW','HOLD','OFF')
        ezca.switch('PZT2_PIT','HOLD','OFF')
        ezca.switch('PZT2_YAW','HOLD','OFF')

        self.timer['wait'] = 5
        chans = ['QPD_{}_OUTPUT'.format(x) for x in ['A_PIT','B_PIT','A_YAW','B_YAW']]
        self.qpd_spots = EzAvg(ezca, 3.0, chans)

        self.pzt_chans = []
        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                self.pzt_chans.append('PZT{}_{}_OUTPUT'.format(pzt,dof))

    @laser_checker
    @pmc_checker
    def run(self):
        if not QPDs_bright():
            if any(ezca[chan] == 0 for chan in self.pzt_chans):
                log('At least one PZT Mirror is off, turning back on')
                return 'PZT_MIRRORS_ON'
            else:
                notify('No light on at least one QPD')
            return
        # Check if spots are centered well enough on QPDs
        done, qpd_vals = self.qpd_spots.ezAvg()
        r_max = 0.7
        if not done:
            return
        if not all(abs(i) < r_max for i in qpd_vals):
            notify('Spots on QPDs need to be centered better')
            return

        if not self.timer['wait']:
            return

        return True

##################################################
class LOCK_QPD_SERVOS(GuardState):
    index = 17
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        #TODO: make PREP_QPD_LOCKING state which sets gains, filters and other.

        # Turn on outputs
        ezca.switch('IP_POS_PIT', 'OUTPUT', 'ON')
        ezca.switch('IP_ANG_PIT', 'OUTPUT', 'ON')
        ezca.switch('IP_POS_YAW', 'OUTPUT', 'ON')
        ezca.switch('IP_ANG_YAW', 'OUTPUT', 'ON')

        # Turn on inputs
        ezca.switch('IP_POS_PIT', 'INPUT', 'ON')
        ezca.switch('IP_ANG_PIT', 'INPUT', 'ON')
        ezca.switch('IP_POS_YAW', 'INPUT', 'ON')
        ezca.switch('IP_ANG_YAW', 'INPUT', 'ON')

        self.timer['wait'] = 2

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):
        if not self.timer['wait']:
            return
        
        # Turn on 20dB
        # JCB 2015/05/27 Removed 20dB because made loops unstable at Y end
        #ezca.switch('IP_POS_PIT', 'FM6', 'ON')
        #ezca.switch('IP_ANG_PIT', 'FM6', 'ON')
        #ezca.switch('IP_POS_YAW', 'FM6', 'ON')
        #ezca.switch('IP_ANG_YAW', 'FM6', 'ON')

        return True

##################################################
class OFFLOAD_QPD_SERVOS(GuardState):
    index = 18
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        # Measure control signals
#        offset_pzt1_pit = ezca['PZT1_PIT_OUTPUT']
#        offset_pzt2_pit = ezca['PZT2_PIT_OUTPUT']
#        offset_pzt1_yaw = ezca['PZT1_YAW_OUTPUT']
#        offset_pzt2_yaw = ezca['PZT2_YAW_OUTPUT']

        chans = []
        # Ramp times
        for pzt in ['PZT1','PZT2']:
            for dof in ['PIT','YAW']:
                ezca[pzt + '_' + dof + '_TRAMP'] = 3
                chans.append('{}_{}_OUTPUT'.format(pzt,dof))
        time.sleep(0.3)

        avgs = ezavg(ezca,2.0,chans)
        ii = 0
        for pzt in ['PZT1','PZT2']:
            for dof in ['PIT','YAW']:
                ezca['{}_{}_OFFSET'.format(pzt,dof)] = avgs[ii]
                ii+=1

        # Offload
#        ezca['PZT1_PIT_OFFSET'] = offset_pzt1_pit
#        ezca['PZT2_PIT_OFFSET'] = offset_pzt2_pit
#        ezca['PZT1_YAW_OFFSET'] = offset_pzt1_yaw
#        ezca['PZT2_YAW_OFFSET'] = offset_pzt2_yaw

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):
        return True

##################################################
class OFFSET_QPD_SERVOS(GuardState):
    index = 19
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        #Reset ALS offsets
        ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4servo[SYSTEM]
        ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4servo[SYSTEM]
        ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4servo[SYSTEM]
        ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4servo[SYSTEM]

        # Turn on pos yaw offset
        for dof in ['PIT','YAW']:
            ezca['IP_POS_' + dof + '_TRAMP'] = 3
            ezca['IP_ANG_' + dof + '_TRAMP'] = 3
            time.sleep(0.3)
            ezca.switch('IP_POS_' + dof, 'OFFSET', 'ON')
            ezca.switch('IP_ANG_' + dof, 'OFFSET', 'ON')
        #ezca['']    

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):
        return True

##################################################
class QPDS_LOCKED(GuardState):
    index = 20

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):
        #tnow = int(gpstime.tconvert('now'))
        #log("{}".format(tnow))
        return True

##################################################
def spiral_waveforms(a, f1, f2, f_sample, t_duration):
    t_sample = 1.0/f_sample
    t = np.arange(0, t_duration, t_sample)
    r = (2*a/np.pi)*np.arcsin(np.sin(2*np.pi*f2*t))
    theta = 2*np.pi*f1*t
    sgn = np.sign(r)
    X = sgn*r*np.sin(theta)
    Y = r*np.cos(theta)
    return X,Y

def offload_test_alignment(sus,dof,test_output,tramp=3.0):
    if sus == 'TMS':
        stage = 'M1'
    else:
        stage = 'M0'
    oagain = ezca[':SUS-{}{}_{}_OPTICALIGN_{}_GAIN'.format(sus,ARM,stage,dof)]
    #test_output = ezca[':SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,ARM,stage,dof)]
    offset = test_output/oagain
    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = tramp
    time.sleep(0.1)
    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_OFFSET'.format(sus,ARM,stage,dof)] += offset
    ezca.switch(':SUS-{}{}_{}_TEST_{}'.format(sus,ARM,stage,dof),'HOLD','OFF')


class SCAN_ALIGNMENT(GuardState):
    index = 52
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def main(self):

        awg.awg_cleanup()
        pdh_ctrl('ON',boost=False)

        for sus in ['TMS','ETM']:
            for dof in ['P','Y']:
                if sus == 'ETM':
                    stage = 'M0'
                else:
                    stage = 'M1'
                ezca.switch(':SUS-{}{}_{}_TEST_{}'.format(sus,ARM,stage,dof),'HOLD','OFF')

        # Set test gains to match optic align gains
        tms.copy_oagain_to_test("P", ezca)
        tms.copy_oagain_to_test("Y", ezca)
        etm.copy_oagain_to_test("P", ezca)
        etm.copy_oagain_to_test("Y", ezca)

        # Channels
        self.chan = {}
        self.chan_rec = {}
        for sus in ["TMS","ETM"]:
            self.chan[sus] = {}
            self.chan_rec[sus] = {}
            if sus=="ETM":
                stage = "M0"
            else:
                stage = "M1"
            for dof in ["P","Y"]:
                self.chan[sus][dof] = 'SUS-{}{}_{}_TEST_{}_'.format(sus,ARM,stage,dof)
                self.chan_rec[sus][dof] = ':SUS-{}{}_{}_TEST_{}_'.format(sus,ARM,stage,dof)

        self.transPdChan = ':ALS-C_TR'+ARM+'_A_LF_OUTPUT'

        #etmAmpl = 1.0 #1.0    #max amplitude (amplitude of triangle wave)
        self.etmFreq1 = 0.2 #0.4		#frequency of sine waves
        self.etmFreq2 = 0.008 #0.016	#frequency of envelope (triangle wave)
        #tmsAmpl = 1.0 #1.0	#max amplitude (amplitude of triangle wave)
        self.tmsFreq1 = 0.1 #0.05		#frequency of sine waves
        self.tmsFreq2 = 0.002 #0.004	#frequency of envelope (triangle wave)
        self.f_s = 16384		#sampling frequency
        self.duration = 125

        #log("Generating waveforms")
        #etmYaw,etmPit = spiral_waveforms(etmAmpl,etmFreq1,etmFreq2,f_s,duration)
        #tmsYaw,tmsPit = spiral_waveforms(tmsAmpl,tmsFreq1,tmsFreq2,f_s,duration)
        #Tuple the waveforms
        #self.wforms = (tmsPit,tmsYaw,etmPit,etmYaw)

        #self.transPD = np.array([])
        #self.etmPitTestOut = np.array([])
        #self.etmYawTestOut = np.array([])
        #self.tmsPitTestOut = np.array([])
        #self.tmsYawTestOut = np.array([])

        #Set-up scan
        #tnow = int(gpstime.tconvert('now'))
        #self.tstart = tnow + 10
        #self.tnext = self.tstart-1
        #log("Start time is {}".format(int(self.tstart)))
        #log("Opening awg streams")

        #self.exc = []
        #for sus in ["TMS","ETM"]:
        #    for dof in ["P","Y"]:
        #        self.exc.append(awg.ArbitraryStream( IFO+":"+self.chan[sus][dof]+"EXC",rate=f_s, start=self.tstart , appinfo="two stream"))
        #for ex in self.exc:
        #    ex.open()

        #self.ii = 0

        self.tappend = 1.0
        #self.step = self.tappend*f_s #FIXME: this needs to be the number of steps (=fs)
        self.step = self.f_s

        self.set_up_stream = True
        self.append_stream = True
        self.scan_started = False
        self.savedata = False
        self.monitor = True
        self.offload_alignment = False
        self.set_from_scan = True
        self.close_stream = True
        self.assess_flashes = True

        tend = gpstime.tconvert('now') + 10
        transPDinit = np.array([])
        while gpstime.tconvert('now') < tend:
            transPDinit = np.append(transPDinit,ezca[self.transPdChan])
        self.max_tpd = np.max(transPDinit)

        self.assess_flashes = True
        self.recheck_tpd = True

        self.scan_count = 0

    #@laser_checker
    #@pmc_checker
    #@assert_qpds_bright
    #@assert_pll_locked
    def run(self):

        # If request isn't PDH_LOCKED or ALIGN_ARM, need to break run loop
        req_state = ezca[':GRD-{}_REQUEST_S'.format(SYSTEM)]
        if (req_state == 'QPDS_LOCKED'):
            log('{} state requested, returning to PREP_PDH_LOCK first'.format(req_state))
            if not self.set_up_stream:
                for exnum in range(len(self.exc)):
                    self.exc[exnum].abort()
                    self.exc[exnum].close()
            return 'PREP_PDH_LOCK'

        if self.scan_count >= 4:
            log('Scan failed to find resonances')
            return 'ALIGN_MANUALLY'

        if self.assess_flashes:
            log('Max transPD val is {0:d}'.format(int(self.max_tpd)))

            if self.max_tpd >= 0.5*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                log('Amplitudes: ETM set to 1.0, TMS set to 2.0')
                self.etmAmpl = 1.0
                self.tmsAmpl = 2.0
            elif 0.5*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM] > self.max_tpd >= 0.3*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                log('Amplitudes: ETM set to 3.0, TMS set to 4.0')
                self.etmAmpl = 3.0
                self.tmsAmpl = 4.0
            elif 0.3*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM] > self.max_tpd >= 0.1*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                log('Amplitudes: ETM set to 5.0, TMS set to 6.0')
                self.etmAmpl = 5.0
                self.tmsAmpl = 6.0
            else:
                log('Amplitudes: ETM set to 7.0, TMS set to 8.0')
                self.etmAmpl = 7.0
                self.tmsAmpl = 8.0

            self.assess_flashes = False


        if self.set_up_stream:

            log("Generating waveforms")
            etmYaw,etmPit = spiral_waveforms(self.etmAmpl,self.etmFreq1,self.etmFreq2,self.f_s,self.duration)
            tmsYaw,tmsPit = spiral_waveforms(self.tmsAmpl,self.tmsFreq1,self.tmsFreq2,self.f_s,self.duration) #TODO: add selfs above
            self.wforms = (tmsPit,tmsYaw,etmPit,etmYaw)

            self.transPD = np.array([])
            self.etmPitTestOut = np.array([])
            self.etmYawTestOut = np.array([])
            self.tmsPitTestOut = np.array([])
            self.tmsYawTestOut = np.array([])

            #Set-up scan
            tnow = int(gpstime.tconvert('now'))
            self.tstart = tnow + 10
            self.tnext = self.tstart-1
            log("Start time is {}".format(int(self.tstart)))
            log("Opening awg streams")

            self.exc = []
            for sus in ["TMS","ETM"]:
                for dof in ["P","Y"]:
                    self.exc.append(awg.ArbitraryStream( IFO+":"+self.chan[sus][dof]+"EXC",rate=self.f_s, start=self.tstart , appinfo="two stream"))
            for ex in self.exc:
                ex.open()

            self.ii = 0

            self.set_up_stream = False


        if self.append_stream:
            dt = self.tnext - self.tstart
            log("Time from t0: %d" % dt )
            end = min(self.ii+self.step, len(self.wforms[0]))
            #Append next part of waveforms to streams
            for exnum in range(len(self.exc)):
                self.exc[exnum].append(self.wforms[exnum][self.ii:end])

            self.ii += self.step
            self.append_stream = False

        if (self.monitor and int(gpstime.tconvert('now')) < self.tnext):
            if not self.scan_started: 
                if gpstime.tconvert('now')>(self.tstart-0.2):
                    log("Scanning and recording data, time is {0}".format(gpstime.tconvert('now')))
                    self.scan_started = True
                time.sleep(0.2)
                return
            elif ezca[self.transPdChan] > alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                log('TMS above threshold, hold')
                ezca.switch(':SUS-ETM{}_M0_TEST_P'.format(ARM),'HOLD','ON')
                ezca.switch(':SUS-ETM{}_M0_TEST_Y'.format(ARM),'HOLD','ON')
                ezca.switch(':SUS-TMS{}_M1_TEST_P'.format(ARM),'HOLD','ON')
                ezca.switch(':SUS-TMS{}_M1_TEST_Y'.format(ARM),'HOLD','ON')
                if self.close_stream:
                    for exnum in range(len(self.exc)):
                        self.exc[exnum].abort()
                        self.exc[exnum].close()
                    self.close_stream = False
                self.monitor = False
                self.offload_alignment = True
                self.set_from_scan = False
                return
            else:
                self.transPD = np.append(self.transPD, ezca[self.transPdChan])
                self.etmPitTestOut = np.append(self.etmPitTestOut, ezca[self.chan_rec["ETM"]["P"]+'OUTPUT'])
                self.etmYawTestOut = np.append(self.etmYawTestOut, ezca[self.chan_rec["ETM"]["Y"]+'OUTPUT'])
                self.tmsPitTestOut = np.append(self.tmsPitTestOut, ezca[self.chan_rec["TMS"]["P"]+'OUTPUT'])
                self.tmsYawTestOut = np.append(self.tmsYawTestOut, ezca[self.chan_rec["TMS"]["Y"]+'OUTPUT'])
                return
        elif (self.monitor and self.ii<len(self.wforms[0])):
            self.tnext += self.tappend
            self.append_stream = True
            return

        log("Finished scan")

        #abort and close stream
        if self.close_stream:
            for exnum in range(len(self.exc)):
                self.exc[exnum].abort()
                self.exc[exnum].close()
            self.close_stream = False


        # TODO: set threshold as well
        if self.set_from_scan and np.max(self.transPD) > 0.1*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
            jj = np.argmax(self.transPD)
            offload_test_alignment('TMS','P',self.tmsPitTestOut[jj],tramp=3.0)
            offload_test_alignment('TMS','Y',self.tmsYawTestOut[jj],tramp=3.0)
            offload_test_alignment('ETM','P',self.etmPitTestOut[jj],tramp=3.0)
            offload_test_alignment('ETM','Y',self.etmYawTestOut[jj],tramp=3.0)
            self.set_from_scan = False


        if self.offload_alignment:
            log('Offloading TEST outputs to OPTICALIGN offsets')
            test_outputs = {}
            for sus in ['TMS','ETM']:
                test_outputs[sus] = {}
                if sus == 'TMS':
                    stage = 'M1'
                else:
                    stage = 'M0'
                for dof in ['P','Y']:
                    test_outputs[sus][dof] = ezca[':SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,ARM,stage,dof)]

            for sus in ['TMS','ETM']:
                for dof in ['P','Y']:
                    offload_test_alignment(sus,dof,test_outputs[sus][dof],tramp=3.0)

            time.sleep(3)
            self.offload_alignment=False

        if self.recheck_tpd:
            tend = gpstime.tconvert('now') + 10
            transPDfinal = np.array([])
            while gpstime.tconvert('now') < tend:
                transPDfinal = np.append(transPDfinal,ezca[self.transPdChan])

            self.max_tpd = np.max(transPDfinal)
            self.recheck_tpd = False

        if self.max_tpd < alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
            log('Flashes are still too low, max trans PD val is {0:d}, rescanning'.format(int(self.max_tpd)))
            self.assess_flashes = True
            self.set_up_stream = True
            self.append_stream = True
            self.scan_started = False
            self.set_from_scan = True
            self.close_stream = True
            self.recheck_tpd = True
            self.monitor = True
            self.scan_count += 1
            return


        if self.savedata:
            filedir = '/data/ASC/InitialAlignment/'+ARM+'ARM/'
            filename = filedir + 'etm{0}_tms{0}_ScanForResonance_{1}.npz'.format(ARM.lower(),int(self.tstart))
            log("Saving data to {}".format(filename))
            np.savez(filename, transPD=self.transPD, etmPitTestOut=self.etmPitTestOut, etmYawTestOut=self.etmYawTestOut, tmsPitTestOut=self.tmsPitTestOut, tmsYawTestOut=self.tmsYawTestOut)
            self.savedata = False

        return True

##################################################
class ALIGN_MANUALLY(GuardState):
    index = 40
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        self.timer['record'] = 10
        self.transPD_array = np.array([])
        self.transPD_chan = ':ALS-C_TR'+ARM+'_A_LF_OUTPUT'

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):

        if not self.timer['record']:
            self.transPD_array = np.append(self.transPD_array,ezca[self.transPD_chan])
            return

        if np.max(self.transPD_array) < 0.1*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
            notify('Arms are too misaligned, align manually')
            # Reset array and timer
            self.transPD_array = np.array([])
            self.timer['record'] = 10
            return

        return True

##################################################
# TODO: add decorator that checks FSS status, and freezes PLL if lost lock
class PREP_PDH_LOCK(GuardState):
    index = 51
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def main(self):

        # In case test outputs weren't offloaded properly
        for sus in ['TMS','ETM']:
            for dof in ['P','Y']:
                if sus == 'ETM':
                    stage = 'M0'
                else:
                    stage = 'M1'
                test_output = ezca[':SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,ARM,stage,dof)]
                if ezca.LIGOFilter(':SUS-{}{}_{}_TEST_{}'.format(sus,ARM,stage,dof)).is_engaged('HOLD') and test_output != 0:
                    offload_test_alignment(sus,dof,test_output,tramp=3.0)

        ezca['REFL_SERVO_IN1GAIN'] = alsArmconfig.pdh_acq_gain[SYSTEM]
        pdh_ctrl('OFF')
        self.timer['unlock'] = 2

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def run(self):
        if not self.timer['unlock']:
            return
        return True

##################################################
class RELOCK_PDH(GuardState):
    index = 53
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def main(self):
        #ezca['REFL_SERVO_IN1GAIN'] = alsArmconfig.pdh_acq_gain[SYSTEM]  #remove for state split AJM20190628

        #pdh_ctrl('ON',boost=False)
        #self.timer['pdhlock'] = 3

        self.relock = True
        self.timer['wait'] = 0

        self.attempt_cnt = 0

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def run(self):

        if not self.timer['wait']:
            return

        # If request isn't PDH_LOCKED or ALIGN_ARM, need to break run loop
        req_state = ezca[':GRD-{}_REQUEST_S'.format(SYSTEM)]
        if not ((req_state == 'PDH_LOCKED') or (req_state =='ALIGN_ARM')):
            log('{} state requested, returning to PREP_PDH_LOCK first'.format(req_state))
            return 'PREP_PDH_LOCK'

        if self.relock:
            self.attempt_cnt += 1
            if self.attempt_cnt >= 6 and alsArmconfig.alignment_scanner[SYSTEM]:
            #if self.attempt_cnt >= 6:
                log('Failed 5 times, scanning alignment')
                return 'SCAN_ALIGNMENT'
            pdh_ctrl('ON',boost=False)
            self.timer['wait'] = 3
            self.relock = False
            return

        if abs(ezca['REFL_SERVO_FASTMON']) > 2.0:
            log('ctrl signal too high, relock')
            pdh_ctrl('OFF')
            self.timer['wait'] = 2
            self.relock = True
            return
            #return 'PREP_PDH_LOCK'

        #if ezca[':ALS-C_TR'+ARM+'_A_LF_OUTPUT'] < alsArmconfig.transpd_threshold[SYSTEM]:
        if ezca[':ALS-C_TR'+ARM+'_A_LF_OUTPUT'] < alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
            log('TPD too low, relock')
            pdh_ctrl('OFF')
            self.timer['wait'] = 2
            self.relock = True
            return
            #return 'PREP_PDH_LOCK'

        return True


##################################################
class RELOCK_PDH_NO_SCAN(GuardState):
    index = 54
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def main(self):
        #ezca['REFL_SERVO_IN1GAIN'] = alsArmconfig.pdh_acq_gain[SYSTEM]  #remove for state split AJM20190628

        #pdh_ctrl('ON',boost=False)
        #self.timer['pdhlock'] = 3

        self.relock = True
        self.timer['wait'] = 0

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    @assert_pll_locked
    def run(self):

        if not self.timer['wait']:
            return

        # If request isn't PDH_LOCKED or ALIGN_ARM, need to break run loop
        req_state = ezca[':GRD-{}_REQUEST_S'.format(SYSTEM)]
        if not ((req_state == 'PDH_LOCKED') or (req_state =='ALIGN_ARM')):
            log('{} state requested, returning to PREP_PDH_LOCK first'.format(req_state))
            return 'PREP_PDH_LOCK'

        if self.relock:
            pdh_ctrl('ON',boost=False)
            self.timer['wait'] = 3
            self.relock = False
            return

        if abs(ezca['REFL_SERVO_FASTMON']) > 2.0:
            log('ctrl signal too high, relock')
            pdh_ctrl('OFF')
            self.timer['wait'] = 2
            self.relock = True
            return
            #return 'PREP_PDH_LOCK'

        #if ezca[':ALS-C_TR'+ARM+'_A_LF_OUTPUT'] < alsArmconfig.transpd_threshold[SYSTEM]:
        if ezca[':ALS-C_TR'+ARM+'_A_LF_OUTPUT'] < alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
            log('TPD too low, relock')
            pdh_ctrl('OFF')
            self.timer['wait'] = 2
            self.relock = True
            return
            #return 'PREP_PDH_LOCK'

        return True


##################################################
class BOOST_PDH_GAIN(GuardState):
    index = 55
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        ezca['REFL_SERVO_COMBOOST'] = 1
        time.sleep(1)
        acqgain = ezca['REFL_SERVO_IN1GAIN']
        nStep = alsArmconfig.pdh_lock_gain[SYSTEM] - acqgain
        cmd_string = str(1) + ',' + str(nStep)
        self.step = cdsutils.Step(ezca,'REFL_SERVO_IN1GAIN',cmd_string,time_step=0.2)

        self.tpd_avg = EzAvg(ezca,3.0,':ALS-C_TR'+ARM+'_A_LF_OUTPUT')

        #chans = [':ALS-C_TR'+ARM+'_A_LF_OUTPUT',':ALS-'+ARM+'_REFL_SERVO_SLOWMON']
        #self.lock_avg = EzAvg(ezca,3.0,chans)

        self.wait = True
        self.timer['wait'] = 0

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):

        if not self.timer['wait']:
            return

        if not self.step.step():
            return

        if self.wait:
            self.timer['wait'] = 1
            self.wait = False
            return

        #[done,vals] = self.lock_avg.ezAvg()
        [done,tpd_val] = self.tpd_avg.ezAvg()
        if not done:
            return

        #tpd_val = vals[0]
        #ctrl_val = vals[1]

        if tpd_val < alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
            log('TPD is low')
            return 'PREP_PDH_LOCK'

        #if ctrl_val > 4:
        #    log('Control signal too large, relock')
        #    return 'PREP_PDH_LOCK'

        return True


##################################################
class PDH_LOCKED(GuardState):
    index = 60
    request = True

    #JCB
    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        channel = ':ALS-C_TR'+ARM+'_A_LF_OUTPUT'

        #self.avg = runningAvg.RunningAvg(ezca, channel, 3)
        self.tpd_avg = EzAvg(ezca,3.0,channel)

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):

        #log("{}".format(alsArmconfig.tms_osc_number[SYSTEM]))

        [done,tpd_val] = self.tpd_avg.ezAvg()
        if not done:
            return
        #if tpd_val > alsArmconfig.transpd_threshold[SYSTEM]:
        if tpd_val > alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
            log('PDH is locked')
            return True
        else:
            return 'PREP_PDH_LOCK'

        #transpd_val, done = self.avg.avg()
        #if done:
        #    if transpd_val > alsArmconfig.transpd_threshold[SYSTEM]:
        #    if tpd_val > alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
        #        log('PDH is locked')
        #        return True
        #    else:
        #        return 'PREP_PDH_LOCK'
        #else:
        #    return False

        #if trans power is below some threshold, jump back to RELOCK_PDH
        #time.sleep(5)
        #if not TransPD_bright():
        #    return 'RELOCK_PDH'
        #return True


##################################################
class PDH_LOCKED_IDLE(GuardState):
    index = 75
    request = False

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):

        return True

##################################################

#tms_osc_row = matrix.asc_ads_actuators_dither['TMS'+ARM]
#tms_demod_sensor = matrix.asc_ads_sensors['GR_TR'+ARM]
#tms_servo_row = matrix.asc_ads_actuators['TMS'+ARM]

#tms_osc_row = 

#asc_ads_actuators_dither[tms]

def turn_on_tms_osc(dof,ampl=200):
    tms_osc_row = matrix.asc_ads_actuators_dither['TMS'+ARM]

    ezca.write(":ASC-ADS_{0}{1}_OSC_TRAMP".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), 5)
    time.sleep(0.1)
    ezca.write(":ASC-ADS_{0}{1}_OSC_FREQ".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), alsArmconfig.tms_osc_freq[SYSTEM][dof])
    ezca.write(":ASC-ADS_{0}{1}_OSC_CLKGAIN".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), ampl)
    ezca.write(":ASC-ADS_LO_{0}_MTRX_{1}_{2}".format(dof,tms_osc_row,alsArmconfig.tms_osc_number[SYSTEM]), 1)


def turn_off_tms_osc():
    tms_osc_row = matrix.asc_ads_actuators_dither['TMS'+ARM]
    # Turn off the TMS pitch and yaw oscillators.
    for dof in ['PIT','YAW']:
        ezca.write(":ASC-ADS_{0}{1}_OSC_TRAMP".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), 2)
        time.sleep(0.1)
        ezca.write(":ASC-ADS_{0}{1}_OSC_CLKGAIN".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), 0)
    time.sleep(2)
    for dof in ['PIT','YAW']:
        ezca.write(":ASC-ADS_{0}{1}_OSC_FREQ".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), 0)
        ezca.write(":ASC-ADS_LO_{0}_MTRX_{1}_{2}".format(dof,tms_osc_row,alsArmconfig.tms_osc_number[SYSTEM]), 0)

def set_tms_demod():
    tms_demod_sensor = matrix.asc_ads_sensors['GR_TR'+ARM]
    for dof in ["PIT","YAW"]:
        ezca.write(":ASC-ADS_{}_SEN_MTRX_{}_{}".format(dof,alsArmconfig.tms_osc_number[SYSTEM],tms_demod_sensor), 1)
        ezca.switch(":ASC-ADS_{}{}_DEMOD_SIG".format(dof,alsArmconfig.tms_osc_number[SYSTEM]),"FM1","ON")
        ezca.write(":ASC-ADS_{}{}_DEMOD_PHASE".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), alsArmconfig.tms_demod_phase[SYSTEM][dof])
        ezca.write(":ASC-ADS_{0}{1}_OSC_SINGAIN".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), 1)
        ezca.write(":ASC-ADS_{0}{1}_OSC_COSGAIN".format(dof,alsArmconfig.tms_osc_number[SYSTEM]), 1)

def set_tms_servo():
    tms_servo_row = matrix.asc_ads_actuators['TMS'+ARM]
    for dof in ["PIT","YAW"]:
        ezca.switch(":ASC-ADS_{}{}_DOF".format(dof,alsArmconfig.tms_osc_number[SYSTEM]),"OUTPUT","ON")
        # Make sure int is on in TMS LOCK banks
        ezca.switch(":SUS-TMS{}_M1_LOCK_{}".format(ARM,dof[0]),"FM2","OFF","FM1","ON")
        ezca.write(":ASC-ADS_{}{}_DOF_GAIN".format(dof,alsArmconfig.tms_osc_number[SYSTEM]),alsArmconfig.tms_dof_gain[SYSTEM][dof])
        ezca.write(":ASC-ADS_OUT_{}_MTRX_{}_{}".format(dof,tms_servo_row,alsArmconfig.tms_osc_number[SYSTEM]),1)


def turn_on_cam_servo(spot):
    for dof in ['PIT','YAW']:
        ezca.write(':ASC-ADS_{}{}_DOF_OFFSET'.format(dof,alsArmconfig.cam_dofs[SYSTEM][spot]),alsArmconfig.cam_offsets[SYSTEM][spot][dof])
        ezca.write(':ASC-ADS_{}{}_DOF_GAIN'.format(dof,alsArmconfig.cam_dofs[SYSTEM][spot]),alsArmconfig.cam_gains[SYSTEM][spot][dof])
        ezca.switch(':ASC-ADS_{}{}_DOF'.format(dof,alsArmconfig.cam_dofs[SYSTEM][spot]),'INPUT','OFFSET','OUTPUT','ON')


def turn_off_align_servos():
    ezca[':ASC-ADS_GAIN'] = 0
    for spot in ['ETM','ITM']:
        for dof in ['PIT','YAW']:
           ezca.switch(':ASC-ADS_{}{}_DOF'.format(dof,alsArmconfig.cam_dofs[SYSTEM][spot]),'OUTPUT','OFF')
           ezca.switch(':ASC-ADS_{}{}_DOF'.format(dof,alsArmconfig.cam_dofs[SYSTEM][spot]),'INPUT','OFFSET','OFF')
    for dof in ['PIT','YAW']:
        ezca.switch(':ASC-ADS_{}{}_DOF'.format(dof,alsArmconfig.tms_osc_number[SYSTEM]),'INPUT','OFF')
    turn_off_tms_osc()


#def offload_suspensions(vals,tramp):
#    ii = 0
#    self.gains = {}
#    for dof in ['P','Y']:
#        self.gains[dof] = {}
#        for sus in ['TMS','ITM','ETM']:
#            if sus == 'TMS':
#                stage = 'M1'
#            else:
#                stage = 'M0'
#            # Offload
#            ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = tramp
#            time.sleep(0.1)
#            ezca[':SUS-{}{}_{}_OPTICALIGN_{}_OFFSET'.format(sus,ARM,stage,dof)] += vals[ii]*alsArmconfig.sus_cals[SYSTEM][sus][dof]
#            if stage == 'M0':
#                stage = 'L1'
#            ezca[':SUS-{}{}_{}_LOCK_{}_TRAMP'.format(sus,ARM,stage,dof)] = tramp
#            time.sleep(0.1)
#            self.gains[dof][sus] = ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)]
#            ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)] = 0
#            time.sleep(0.1)
#            ii+=1

########################
class ALIGN_ARM(GuardState):
    index = 70
    request = True

    #JCB
    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def main(self):

        #if ezca[':ALS-C_TR{}_A_LF_OUTPUT'.format(ARM)] < alsArmconfig.transpd_threshold[SYSTEM]:
        if ezca[':ALS-C_TR{}_A_LF_OUTPUT'.format(ARM)] < alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
            log('PDH dropped lock')
            return 'PREP_PDH_LOCK'

        turn_on_tms_osc('PIT',ampl=200)
        turn_on_tms_osc('YAW',ampl=100)

        set_tms_demod()
        set_tms_servo()

        time.sleep(4)        

        turn_on_cam_servo('ETM')
        turn_on_cam_servo('ITM')
        # Note: output matrices set by other guardians, ITM servo not necessarily turned on

        # Turn on servo
        for dof in ['PIT','YAW']:
            ezca.switch(":ASC-ADS_{}{}_DOF".format(dof,alsArmconfig.tms_osc_number[SYSTEM]),"INPUT","ON")

        ezca[':ASC-ADS_GAIN'] = 1.0

        self.tpdAvg = EzAvg(ezca,1,':ALS-C_TR{}_A_LF_OUTPUT'.format(ARM))

        #-------------------------------------------------
        chans = []
        for dof in ['P','Y']:
            for sus in ['TMS','ITM','ETM']:
                if sus == 'TMS':
                    stage = 'M1'
                else:
                    stage = 'L1'
                chans.append(':SUS-{}{}_{}_LOCK_{}_OUTPUT'.format(sus,ARM,stage,dof))

        # Record control signals 2 seconds in the past
        self.ctrlSignals = EzAvg(ezca,3,chans,tDelay=3)
        #-------------------------------------------------

        #self.timer['mode_check'] = 30

    @laser_checker
    @pmc_checker
    @assert_qpds_bright
    def run(self):

        #TODO: set timer to get above 1200, relock if it doesnt get above

        #[tpd_done,tpd_val] = self.tpdAvg.ezAvg()
        #if not tpd_done:
        #    return

        #if tpd_val < alsArmconfig.transpd_threshold[SYSTEM]:
        #if tpd_val < alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
        #     log('Arm transmission below threshold, turning off servos')
        #     turn_off_align_servos()
        #     return 'PREP_PDH_LOCK'

        [do_offload,vals] = self.ctrlSignals.ezAvg()

        if ezca[':ALS-C_TR{}_A_LF_OUTPUT'.format(ARM)] < alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]:
            log('Arm transmission below threshold, turning off servos')
            turn_off_align_servos()
            #-------------------------------
            if do_offload:
                log('Offloading control signals')
                tramp = 3
                ii = 0
                self.gains = {}
                for dof in ['P','Y']:
                    self.gains[dof] = {}
                    for sus in ['TMS','ITM','ETM']:
                        if sus == 'TMS':
                            stage = 'M1'
                        else:
                            stage = 'M0'
                        # Offload
                        ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = tramp
                        time.sleep(0.1)
                        ezca[':SUS-{}{}_{}_OPTICALIGN_{}_OFFSET'.format(sus,ARM,stage,dof)] += vals[ii]*alsArmconfig.sus_cals[SYSTEM][sus][dof]
                        if stage == 'M0':
                            stage = 'L1'
                        ezca[':SUS-{}{}_{}_LOCK_{}_TRAMP'.format(sus,ARM,stage,dof)] = tramp
                        time.sleep(0.1)
                        self.gains[dof][sus] = ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)]
                        ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)] = 0
                        time.sleep(0.3)
                        ii+=1

                time.sleep(tramp)

                for dof in ['P','Y']:
                    for sus in ['TMS','ITM','ETM']:
                        if sus == 'TMS':
                            stage = 'M1'
                        else:
                            stage = 'L1'
                        ezca[':SUS-{}{}_{}_LOCK_{}_RSET'.format(sus,ARM,stage,dof)] = 2
                        ezca[':SUS-{}{}_{}_LOCK_{}_TRAMP'.format(sus,ARM,stage,dof)] = 0
                        time.sleep(0.1)
                        ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)] = self.gains[dof][sus]
                        if stage == 'L1':
                            stage = 'M0'
                        ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = 1
            #-------------------------------

            return 'PREP_PDH_LOCK'

        #elif self.timer['mode_check'] and \
        # (alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM] > tpd_val > alsArmconfig.tpd_threshold['ACQUIRE'][SYSTEM]):
        #     log('Might be locked on wrong mode, relocking')
        #     return 'PREP_PDH_LOCK'

        return True

##################################################
class TURN_OFF_ALIGN_SERVOS(GuardState):
    index = 72
    request = False

    @laser_checker
    @pmc_checker
    def main(self):

        #TODO: Write offsets to a file, to be offloaded.

        log('Turning off alignment servos')
        turn_off_align_servos()

        chans = []
        for dof in ['P','Y']:
            for sus in ['TMS','ITM','ETM']:
                if sus == 'TMS':
                    stage = 'M1'
                else:
                    stage = 'L1'
                chans.append(':SUS-{}{}_{}_LOCK_{}_OUTPUT'.format(sus,ARM,stage,dof))

        self.tramp = 6
        self.ctrlSignals = EzAvg(ezca,10,chans)
        self.offload = True
        self.clear_histories = False
        self.timer['wait'] = 0

    @laser_checker
    @pmc_checker
    def run(self):

        if not self.timer['wait']:
            return

        [done,vals] = self.ctrlSignals.ezAvg()
        if not done:
            #TODO: If cavity drops lock jump out of this state
            return

        #Offload and clear filters
        #TODO: ramp lock filters down nicely
        if self.offload:
            log('Offloading control signals')
            ii = 0
            self.gains = {}
            for dof in ['P','Y']:
                self.gains[dof] = {}
                for sus in ['TMS','ITM','ETM']:
                    if sus == 'TMS':
                        stage = 'M1'
                    else:
                        stage = 'M0'
                    # Offload
                    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = self.tramp
                    time.sleep(0.1)
                    # Offset added
                    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_OFFSET'.format(sus,ARM,stage,dof)] += vals[ii]*alsArmconfig.sus_cals[SYSTEM][sus][dof]
                    if stage == 'M0':
                        stage = 'L1'
                    ezca[':SUS-{}{}_{}_LOCK_{}_TRAMP'.format(sus,ARM,stage,dof)] = self.tramp
                    time.sleep(0.1)
                    #Lock gain set to zero
                    self.gains[dof][sus] = ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)]
                    ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)] = 0
                    time.sleep(0.1)
                    ii+=1
            self.timer['wait'] = self.tramp
            self.offload = False
            self.clear_histories = True
            return

        if self.clear_histories:
            for dof in ['P','Y']:
                for sus in ['TMS','ITM','ETM']:
                    if sus == 'TMS':
                        stage = 'M1'
                    else:
                        stage = 'L1'
                    ezca[':SUS-{}{}_{}_LOCK_{}_RSET'.format(sus,ARM,stage,dof)] = 2
                    ezca[':SUS-{}{}_{}_LOCK_{}_TRAMP'.format(sus,ARM,stage,dof)] = 0
                    time.sleep(0.1)
                    ezca[':SUS-{}{}_{}_LOCK_{}_GAIN'.format(sus,ARM,stage,dof)] = self.gains[dof][sus]
                    if stage == 'L1':
                        stage = 'M0'
                    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = 1
            self.clear_histories = False

        return True



##################################################
class COARSE_SCAN_PLL(GuardState):
    index = 27
    request = False

    @laser_checker
    @pmc_checker
    def main(self):

        pdh_ctrl('OFF')

        #Ensure Beckhoff PLL Autolcker is Disabled
        ezca['FIBR_LOCK_LOGIC_ENABLE'] = 0

        #Turn off temperature feedback
        ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0


        beatAmplChan = 'FIBR_A_DEMOD_RFMON'
        beatFreqChan = 'FIBR_LOCK_BEAT_FREQUENCY'
        self.crystalFreqChan = 'LASER_HEAD_CRYSTALFREQUENCY'

        fstart = -700
        fstop = 700
        nsteps = 8

        # Will carry out a coarse scan from -700 to +700 of 8 steps
        self.laserScan = LaserFreqScan(ezca,fstart,fstop,nsteps,self.crystalFreqChan,beatAmplChan,beatFreqChan,3)

        #self.do_once = True

    @laser_checker
    @pmc_checker
    def run(self):

        [done,crystalFreqCoarse,beatAmplCoarse,beatFreqCoarse] = self.laserScan.scan()

        if not done:
            return

        #if self.do_once:
        #    self.do_once = False

        index_beatmax = np.argmax(beatAmplCoarse)
        crystalFreq_beatmax = crystalFreqCoarse[index_beatmax]
        # Place the crystal frequency at the place where the beat amplitude is maximum. 
        ezca[self.crystalFreqChan] = crystalFreq_beatmax
        return True


##################################################
class FINE_SCAN_PLL(GuardState):
    index = 28
    request = False

    @laser_checker
    @pmc_checker
    def main(self):

        pdh_ctrl('OFF')

        #Ensure Beckhoff PLL Autolcker is Disabled
        ezca['FIBR_LOCK_LOGIC_ENABLE'] = 0

        #Turn off temperature feedback
        ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0

        beatAmplChan = 'FIBR_A_DEMOD_RFMON'
        beatFreqChan = 'FIBR_LOCK_BEAT_FREQUENCY'
        self.crystalFreqChan = 'LASER_HEAD_CRYSTALFREQUENCY'

        #fstart = crystalFreq_beatmax-100
        #fstop = crystalFreq_beatmax+100
        fstart = ezca[self.crystalFreqChan]-100
        fstop = ezca[self.crystalFreqChan]+100
        nsteps = 6

        # Will carry out a fine scan from -100 to +100 of 6 steps
        self.laserScan = LaserFreqScan(ezca,fstart,fstop,nsteps,self.crystalFreqChan,beatAmplChan,beatFreqChan,5)

        #self.do_once = True

    @laser_checker
    @pmc_checker
    def run(self):

        [done,crystalFreqFine,beatAmplFine,beatFreqFine] = self.laserScan.scan()
        if not done:
            return

        #if self.do_once:
        #    self.do_once = False

        # Give the beat frequency (which is the absolute of the frequency difference) a sign from the beat amplitude slope
        beatAmplSlope = np.gradient(beatAmplFine)
        beatSignedFreq = np.multiply(beatFreqFine, np.sign(beatAmplSlope))

        # Fit with a simple poly1d fit (i.e. a line), then find crystal frequency that gives the correct beatnote frequency
        fit_coeff = np.polyfit(beatSignedFreq,crystalFreqFine,1)
        p = np.poly1d(fit_coeff)
        crystalFreqFinal = p(alsArmconfig.beatnote_set[SYSTEM])
        ezca[self.crystalFreqChan] = crystalFreqFinal
        return True

##################################################
class WAIT_FOR_PMC(GuardState):
    index = 29
    request = False

    @laser_checker
    def main(self):

        #Turn off PLL loops
        ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0
        ezca['FIBR_SERVO_IN1EN'] = 0

    @laser_checker
    def run(self):

        if pmc_unlocked():
            notify('Waiting for PMC to relock')
            return

        return True

##################################################
class LOCK_PLL(GuardState):
    index = 30
    request = False

    @laser_checker
    def main(self):

        # Ensure PLL servo is on
        ezca['FIBR_SERVO_IN1EN'] = 1
        self.timer['pll'] = 5

        self.do_once = True

    @laser_checker
    def run(self):

        if not self.timer['pll']:
            return

        if self.do_once:
            self.do_once = False
            ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 1
            self.timer['pll'] = 15
            return
    
        return True


##################################################
##################################################
edges = [
    ('INIT', 'RELEASE_GREEN_PHOTONS'),
    ('INIT', 'TURN_OFF_QPD_SERVOS'),

    ('UNLOCK_PDH', 'TURN_OFF_QPD_SERVOS'),
    ('TURN_OFF_QPD_SERVOS', 'UNLOCK_PLL'),
    ('UNLOCK_PLL', 'SHUTTER_GREEN'),
    ('SHUTTER_GREEN', 'SHUTTERED'),
    ('SHUTTERED','PZT_MIRRORS_OFF'),
    ('PZT_MIRRORS_OFF','PZT_MIRRORS_ON'),
    ('PZT_MIRRORS_ON','SHUTTERED'),
    ('SHUTTERED', 'RELEASE_GREEN_PHOTONS'),
    ('LASER_OFF','RELEASE_GREEN_PHOTONS'),
    ('RELEASE_GREEN_PHOTONS', 'RESET_QPD_SERVOS'),
    ('RESET_QPD_SERVOS', 'LOCK_QPD_SERVOS'),
    ('LOCK_QPD_SERVOS', 'OFFLOAD_QPD_SERVOS'),
    ('OFFLOAD_QPD_SERVOS', 'OFFSET_QPD_SERVOS'),
    ('OFFSET_QPD_SERVOS','QPDS_LOCKED'),
    #('QPDS_LOCKED', 'RELOCK_PDH'),
    ('ALIGN_MANUALLY', 'PREP_PDH_LOCK'),
    ('QPDS_LOCKED', 'PREP_PDH_LOCK'),
    ('PREP_PDH_LOCK', 'RELOCK_PDH'),
    ('RELOCK_PDH', 'BOOST_PDH_GAIN'),
    ('RELOCK_PDH', 'PREP_PDH_LOCK'),
    ('PREP_PDH_LOCK', 'RELOCK_PDH_NO_SCAN',2),
    ('RELOCK_PDH_NO_SCAN', 'BOOST_PDH_GAIN'),
    ('RELOCK_PDH_NO_SCAN', 'PREP_PDH_LOCK'),
    ('PREP_PDH_LOCK', 'SCAN_ALIGNMENT',3),
    ('SCAN_ALIGNMENT', 'RELOCK_PDH'),
    ('BOOST_PDH_GAIN', 'PDH_LOCKED'),
    ('PDH_LOCKED', 'ALIGN_ARM'),
    ('ALIGN_ARM', 'TURN_OFF_ALIGN_SERVOS'),
    ('TURN_OFF_ALIGN_SERVOS', 'PDH_LOCKED'),
    #('PDH_LOCKED','QPDS_LOCKED'),   # AJM 190618 should have it go through UNLOCK_PDH first
    ('PDH_LOCKED','PREP_PDH_LOCK'),
    ('PREP_PDH_LOCK','QPDS_LOCKED'),
    ('PDH_LOCKED','PDH_LOCKED_IDLE'),
    ('PDH_LOCKED_IDLE','PDH_LOCKED'),
    ('PDH_LOCKED_IDLE', 'UNLOCK_PDH'),
    ('UNLOCK_PDH_GOTO','TURN_OFF_QPD_SERVOS'),
    #('PDH_LOCKED', 'RELOCK_PDH'),
    #('RELOCK_PDH', 'UNLOCK_PDH'), # TH 9/14/18 added for RH tests, to avoid stuck in PDH loop when trying to shutter (won't work AJM)
    ('QPDS_LOCKED', 'UNLOCK_PDH'), # TH same as above
    ('COARSE_SCAN_PLL','FINE_SCAN_PLL'),
    ('FINE_SCAN_PLL','LOCK_PLL'),
    ('WAIT_FOR_PMC','LOCK_PLL'),
    #('LOCK_PLL','RELOCK_PDH'),
    ('LOCK_PLL','PREP_PDH_LOCK'),
    ]
